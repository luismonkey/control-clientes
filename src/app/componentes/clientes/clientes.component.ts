import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ClienteServicio } from 'src/app/servicios/cliente.service';
import { Cliente } from 'src/app/modelos/cliente.model';
import { FlashMessagesService } from 'angular2-flash-messages';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-clientes',
  templateUrl: './clientes.component.html',
  styleUrls: ['./clientes.component.css']
})
export class ClientesComponent implements OnInit {

  clientes: Cliente[];
  cliente: Cliente = {
    nombre: '',
    apellido: '',
    email: '',
    saldo: 0
  }

  @ViewChild("clienteForm",{static: false}) clienteForm: NgForm;

  @ViewChild("botonCerrar",{static: false}) botonCerrar: ElementRef;

  constructor(private clienteServicio: ClienteServicio, private flashMessages: FlashMessagesService) { }

  ngOnInit() {
    //recuperamos el listado de clientes
    this.clienteServicio.getClientes().subscribe(
      cliente => {
        this.clientes = cliente;
      }
    )
  }

  getSaldoTotal(){
    let saldoTotal: number = 0;
    if(this.clientes){
      this.clientes.forEach( cliente =>{
        saldoTotal += cliente.saldo;
      })
    }return saldoTotal;
  }

  agregar({value, valid}: {value:Cliente, valid: boolean}){
    if(!valid){
      this.flashMessages.show('Por favor, debe completar el formulario correctamente', {
        cssClass: 'alert-danger',
        timeout: 4000
      });
    }else{
       //agregar cliente
       this.clienteServicio.agregarCliente(value);
       this.clienteForm.resetForm();
       this.cerrarModal();
    }
  }

  private cerrarModal(){
    this.botonCerrar.nativeElement.click();
  }

}
