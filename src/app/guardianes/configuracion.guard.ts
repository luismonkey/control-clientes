import { CanActivate, Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { ConfiguracionServicio } from '../servicios/configuracion.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class ConfiguracionGUard implements CanActivate{
    constructor(private router: Router, private configServicio: ConfiguracionServicio ){}

    canActivate(): Observable<boolean>{
        return this.configServicio.getConfiguracion().pipe(
            map(configuracion => {
                if(configuracion.permitirRegistro){
                    return true;
                }else{
                    this.router.navigate(['/login']);
                    return false;
                }
            })
        )
    }
}