// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firestore:{
    apiKey: "AIzaSyAaqrfXuGsNbxI2mmBCPirDzdpgzxbKZLc",
    authDomain: "control-clientes-81c1e.firebaseapp.com",
    databaseURL: "https://control-clientes-81c1e.firebaseio.com",
    projectId: "control-clientes-81c1e",
    storageBucket: "control-clientes-81c1e.appspot.com",
    messagingSenderId: "628216727623",
    appId: "1:628216727623:web:3b9d456b91723970b20962",
    measurementId: "G-WVEFD87J2D"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
